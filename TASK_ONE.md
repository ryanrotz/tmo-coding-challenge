# Task 1

Please provide a short code review of the base master branch:

What is done well?
What would you change?
Are there any code smells or problematic implementations?

Make a PR to fix at least one of the issues that you identify

## Missing Requirements

There are some features missing that were included in the problem statement:

- _User enters exchange name (NYSE, NASDAQ, Tokyo Stock Exchange,...)_

  - This input field is missing

- _Select date range to gather data for._

  - A date range is missing. There is a dropdown but that does not meet the requirement. I would clarify this with the business owner before starting work.

- _Collect stock price info for specified stock symbol during date range._

  _if less than 1 week then show hourly price changes_

  _if less than 1 month then show daily high/low and close values_

  _if more than 1 month then show close values._

  - This would be dependent on the data from the date range fields, which would be passed as parameters in the API call. The graph would change how it displayed the data either

    a) based on the response data (preferred), or

    b) by setting a property in the store (e.g. “lessThanOneWk = true”).

## Code Review

### What is done well?

The basic html such as the input fields and call-to-action button are set up and functioning properly. The API fires when the button is clicked and the correct parameters are added to the GET URL.

### Angular libraries

This app makes uses its own homemade [libraries](https://angular.io/guide/creating-libraries), which I believe came from Angular 6? Pretty cool! This makes sense if you’re planning to use data-access-ap-config and data-access-price-query in other applications or you want to share it with other developers, either at T-Mobile or via npm. If that’s not the case, these libraries might be overkill and introduce more complexity than needed.

### Hard-coded fields for the chart

In the data-access-price-query library, in the PriceQueryFacade class, the date and closing price are hardcoded. It may be better to make these dynamic so that the chart can be more flexible. For example, if a user wants to see a stock’s opening price or changePercent instead. This could be put into a configuration file or built into a schematic to be used with the library, instead of being hardcoded.

### The API URL needed to be changed in environment.ts. (FIXED)

"sandbox" needed to be changed to "cloud" in the apiURL.

### The chart was missing! (FIXED)

The data being passed to the google chart needed to be changed from “data” to “data\$ | async”

## UI/UX

- I’d like to see the input field for symbols (i.e. AAPL) display a list of suggestions onclick/ontouch and make more accurate suggestions as a user types. This is expected by users today.
